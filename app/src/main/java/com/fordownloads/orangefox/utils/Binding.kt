package com.fordownloads.orangefox.utils

import android.view.ContextThemeWrapper
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter
import com.fordownloads.orangefox.R

@BindingAdapter("layout_marginBottom")
fun setLayoutMarginBottom(view: View, dimen: Int) {
    view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
        bottomMargin = (view.context.resources.displayMetrics.density * dimen).toInt()
    }
}

@BindingAdapter("layout_marginHorizontal")
fun setLayoutMarginHorizontal(view: View, dimen: Int) {
    val padding = (view.context.resources.displayMetrics.density * dimen).toInt()
    view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
        leftMargin = padding
        rightMargin = padding
    }
}

@BindingAdapter("visible")
fun setVisible(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}

@BindingAdapter("clipToOutline")
fun setClipToOutline(view: View, value: Boolean) {
    view.clipToOutline = value
}

@BindingAdapter("table")
fun setTable(table: TableLayout, content: Map<Int, String?>) {
    with (table.context) {
        table.removeAllViews()
        for (entry in content.entries)
            table.addView(TableRow(this).apply {
                entry.value?.let {
                    addView(TextView(ContextThemeWrapper(this@with, R.style.Text_Gray)).apply { text = getString(entry.key) })
                    addView(TextView(ContextThemeWrapper(this@with, R.style.Text_Bold)).apply { text = it })
                }
            })
    }
}