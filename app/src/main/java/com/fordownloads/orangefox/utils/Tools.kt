package com.fordownloads.orangefox.utils

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.drawable.AnimatedVectorDrawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import android.view.View
import android.view.ViewPropertyAnimator
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.viewbinding.BuildConfig
import com.fordownloads.orangefox.Const
import com.fordownloads.orangefox.Prefs
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.ScreenSize
import com.fordownloads.orangefox.api.RequestAPI
import com.fordownloads.orangefox.api.Variant
import com.fordownloads.orangefox.base.SheetBase
import com.fordownloads.orangefox.databinding.SheetShareBinding
import com.fordownloads.orangefox.ui.main.ActivityMain
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream
import java.math.BigInteger
import java.security.MessageDigest
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


var View.visible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

object Tools {
    fun String.cap() =
        replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

    fun getORS() = if (Const.DIR_CACHE.isDirectory) Const.ORS_FILE else "/data" + Const.ORS_FILE
    
    @SuppressLint("SimpleDateFormat")
    fun getBackupFileName() =
        SimpleDateFormat("yyyy-MM-dd--HH-mm-ss_").format(Date()) + Build.DEVICE

    fun Context.openUrl(url: String?) = try {
            startActivity(Intent(Intent.ACTION_VIEW)
                .setData(Uri.parse(url))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        } catch (e: Exception) {
            Toast.makeText(applicationContext, getString(R.string.err_link, url), Toast.LENGTH_LONG).show()
        }

    @SuppressLint("PrivateApi")
    fun getSystemProperty(key: String): String {
        try {
            return Class.forName("android.os.SystemProperties")
                .getMethod("get", String::class.java).invoke(null, key) as String
        } catch (e: Exception) {}
        return ""
    }

    inline fun <reified T> jsonToObjSafe(str: String?): T? {
        if (str == null) return null
        try {
            return RequestAPI.gson.fromJson(str, T::class.java)
        } catch (e: Exception) {
            reportException(e)
        }
        return null
    }

    fun ImageView.startAnimation() = (drawable as AnimatedVectorDrawable).start()
    fun ImageView.stopAnimation() = (drawable as AnimatedVectorDrawable).stop()

    fun FragmentManager.replace(fragment: Fragment, backAllowed: Boolean = true) = commit {
        setCustomAnimations(
            R.anim.fragment_in, R.anim.fragment_out,
            R.anim.fragment_in_back, R.anim.fragment_out_back
        )
        replace(R.id.fm, fragment)
        if (backAllowed)
            addToBackStack(null)
    }

    fun Activity.color(id: Int) = ContextCompat.getColor(this, id)
    fun Long.formatDate(): String = DateFormat.getDateTimeInstance().format(Date(this * 1000))
    fun String.formatDate(): String? =
        SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.ENGLISH).parse(this)?.let {
            DateFormat.getDateTimeInstance().format(it)
        }

    @SuppressLint("SdCardPath")
    fun getFileFromFilePicker(intent: Intent): String? {
        val file = File(intent.data?.path ?: return null)
        val path = file.absolutePath
        if (file.exists()) return path

        val uri = path.split(":").toTypedArray()
        uri[0] = uri[0].replace("/document/", "")

        return when {
            uri.size != 2 || uri[0] == "msf" -> null //maybe this will improve performance (?)
            uri[0] == "primary" && File("/sdcard/${uri[1]}").exists() -> "/sdcard/${uri[1]}"
            File("/storage/${uri[0]}/${uri[1]}").exists() -> "/external_sd/${uri[1]}"
            File("/mnt/media_rw/${uri[0]}/${uri[1]}").exists() -> "/usb_otg/${uri[1]}"
            else -> null
        }
    }

    fun getUrlMirror(prefs: Prefs, mirrors: HashMap<String, String>?) =
        if (mirrors.isNullOrEmpty())
            null
        else
            mirrors[prefs.mirrorCode] ?: mirrors.values.first()

    fun reportException(e: Throwable, important: Boolean = true) {
        if (!BuildConfig.DEBUG && important)
            e.printStackTrace()
    }

    fun checkMD5(md5: String, updateFile: File): Boolean {
        val digest = MessageDigest.getInstance("MD5")
        val buffer = ByteArray(8192)
        var read: Int
        return FileInputStream(updateFile).use { stream ->
            while (stream.read(buffer).also { read = it } > 0)
                digest.update(buffer, 0, read)

            String.format("%32s", BigInteger(1, digest.digest()).toString(16))
                .replace(' ', '0').equals(md5, ignoreCase = true)
        }
    }

    fun Context.getScreenSize() =
        resources.displayMetrics.let { ScreenSize(it.widthPixels, it.heightPixels) }

    fun Fragment.isLandscape() =
        resources.displayMetrics.let {
            resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE ||
                    it.widthPixels.toFloat() / it.heightPixels.toFloat() > 1.6
        }

    @SuppressLint("ShowToast")
    fun Activity.getSnackbar(msg: Int, view: View, attach: Boolean = true): Snackbar {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
            .setActionTextColor(ContextCompat.getColor(this, R.color.fox_accent))
            .setBackgroundTint(ContextCompat.getColor(this, R.color.fox_card))
            .setTextColor(ContextCompat.getColor(this, R.color.white))
            .setDuration(6000).let { return if (attach) it.setAnchorView(view) else it }
    }

    fun Activity.getSnackbar(msg: Int, dialog: BottomSheetDialog?): Snackbar {
        dialog?.dismiss()
        return getSnackbar(msg, findViewById<View>(R.id.layout))
    }

    fun Activity.showSnackbar(msg: Int, view: View, attach: Boolean = true) = getSnackbar(msg, view, attach).show()
    fun Activity.showSnackbar(msg: Int, dialog: BottomSheetDialog?) = getSnackbar(msg, dialog).show()

    fun Activity.share(prefs: Prefs, fileName: String, log: File) {
        if (prefs.dialogShareShown)
            shareNoDialog(fileName, log)
        else
            SheetBase(this).apply {
                body.title = getString(R.string.log_share)
                SheetShareBinding.inflate(layoutInflater, body.inner, true).apply {
                    ok.setOnClickListener {
                        prefs.dialogShareShown = true
                        shareNoDialog(fileName, log)
                        dismiss()
                    }
                }
                show()
            }
    }

    private fun Activity.shareNoDialog(fileName: String, log: File) {
        val mime = if (fileName.endsWith(".zip")) "application/zip" else "text/plain"
        val uri = FileProvider.getUriForFile(this, "com.fordownloads.orangefox.fileprovider", log)
        startActivity(Intent.createChooser(Intent(Intent.ACTION_SEND).apply {
            type = mime
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            clipData = ClipData(fileName, arrayOf(mime), ClipData.Item(uri))
            putExtra(Intent.EXTRA_STREAM, uri)
        }, getString(R.string.share)))
    }

    fun View.fade(a: Float, listener: AnimatorListenerAdapter = (object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator) { alpha = a }
    })): ViewPropertyAnimator = animate().alpha(a).setListener(listener).setDuration(200)

    fun Context.hasStoragePermission() =
        checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
              checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

    fun hasStoragePermissionScoped() =
        Build.VERSION.SDK_INT < Build.VERSION_CODES.R || Environment.isExternalStorageManager()

    // executes when user decides to revoke storage permission
    fun Activity.doYeetIfNoPM(): Boolean {
        if (!hasStoragePermission() || !hasStoragePermissionScoped()) {
            startActivity(Intent(this, ActivityMain::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            Log.e(Const.TAG, "App killed")
            return true
        }
        return false
    }

    fun Context.isClassicNavigation(): Boolean {
        return try {
            val id = resources.getIdentifier("config_navBarInteractionMode", "integer", "android")
            return if (id > 0) resources.getInteger(id) != 2 else true
        } catch (e: Resources.NotFoundException) {
            true
        }
    }

    fun ActivityResultCaller.activityCallback(callback: ActivityResultCallback<ActivityResult>) =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult(), callback)

    fun isServerError(code: Int) = code >= 500
}