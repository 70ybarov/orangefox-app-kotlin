package com.fordownloads.orangefox

import android.annotation.SuppressLint
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.fordownloads.orangefox.api.Variant
import com.fordownloads.orangefox.databinding.ItemListBinding
import kotlinx.parcelize.Parcelize

@Parcelize
data class RecyclerItem(val title: String, val subtitle: String, val icon: Int, val data: String? = null,
                        val searchData: String? = null) : Parcelable
@Parcelize
data class ReleaseToInstall(val version: String, val type: String, val variant: Variant) : Parcelable
data class ScreenSize(val w: Int, val h: Int)

open class RecyclerAdapter(
    private val items: MutableList<RecyclerItem>,
    private val listener: ((ib: ItemListBinding, position: Int) -> Unit)? = null,
    private val listenerHold: ((ib: ItemListBinding, position: Int) -> Unit)? = null
    ) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    inner class ViewHolder(val b: ItemListBinding) : RecyclerView.ViewHolder(b.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(vh: ViewHolder, pos: Int) {
        vh.b.item = items[pos]
        listener?.let { vh.b.root.setOnClickListener { it(vh.b, pos) } }
        listenerHold?.let {
            vh.b.setHoldAction { it(vh.b, pos) }
            vh.b.root.setOnLongClickListener { it(vh.b, pos); true }
        }
    }

    override fun getItemCount() = items.size

    @SuppressLint("NotifyDataSetChanged")
    fun setSearchField(v: EditText) {
        val all = arrayListOf<RecyclerItem>() + items
        v.doOnTextChanged { text, _, _, _ ->
            items.clear()
            items += if (text?.isEmpty() != false) all else all.filter { it.searchData?.contains(text.toString(), true) ?: false }
            notifyDataSetChanged()
        }
    }
}