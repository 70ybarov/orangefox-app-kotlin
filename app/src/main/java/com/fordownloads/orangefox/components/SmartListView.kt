package com.fordownloads.orangefox.components

import android.content.Context
import android.widget.ListView

class SmartListView(context: Context) : ListView(context) {
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(
            Int.MAX_VALUE shr 2, MeasureSpec.AT_MOST
        ))
    }
}