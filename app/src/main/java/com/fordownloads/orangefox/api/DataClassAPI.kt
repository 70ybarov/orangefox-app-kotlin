package com.fordownloads.orangefox.api

import android.os.Parcelable
import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class ReleaseInstalled (
    @Expose var codename: String? = null,
    @Expose var type: String? = null,
    @Expose var version: String? = null,
    @Expose var commit: String? = null,
    @Expose var date: String? = null,
    @Expose var branch: Int? = null,
    @Expose var variant: String? = null,
    @Expose var release_id: String? = null,
)

/*
 * DEVICE
 */

data class Device (
    @Expose @SerializedName("_id") var id: String? = null,
    @Expose var codenames: List<String>? = null,
    @Expose var model_names: List<String>? = null,
    @Expose var oem_name: String? = null,
    @Expose var title: String? = null,
    @Expose var supported: Boolean = false,
    @Expose var url: String? = null,
    @Expose var installation_instruction: String? = null,
    @Expose var maintainer: MaintainerShort? = null,
    @Expose var ab_device: Boolean = false,
    @Expose var public_notes: String? = null,
    @Expose var support: DeviceSupport? = null,
    @Expose var sources: String? = null
)

data class DeviceShort (
    @Expose @SerializedName("_id") var id: String? = null,
    @Expose var codenames: List<String>? = null,
    @Expose var model_names: List<String>? = null,
    @Expose var oem_name: String? = null,
    @Expose var title: String? = null,
    @Expose var supported: Boolean = false
)

data class DeviceRequest (
    @Expose var data: List<DeviceShort>? = null
)

data class DeviceSupport (
    @Expose var telegram_chat: String? = null,
    @Expose var forum: String? = null
)

/*
 * MAINTAINER
 */

data class MaintainerShort (
    @Expose @SerializedName("_id") var id: String? = null,
    @Expose var name: String? = null
)

/*
 * RELEASE
 */

data class ReleaseRequest (
    @Expose var data: List<ReleaseShort>? = null
)

data class Release (
    @Expose @SerializedName("_id") var id: String? = null,
    @Expose var device_id: String? = null,
    @Expose var date: Long = 0,
    @Expose var version: String? = null,
    @Expose var type: String? = null,
    @Expose var changelog: List<String>? = null,
    @Expose var bugs: List<String>? = null,
    @Expose var notes: String? = null,
    @Expose var url: String? = null,
    //@Expose var recovery_img: ReleaseRecoveryImg? = null,
    @Expose var variants: JsonObject? = null
)

@Parcelize
data class Variant (
    var variant_name: String? = null, //does not exist in API, needs for transferring info in app
    @Expose var variant_id: String? = null,
    @Expose var release_id: String? = null,
    @Expose var mirrors: HashMap<String, String>? = null,
    @Expose var size: Int = 0,
    @Expose var md5: String = "",
    @Expose var filename: String = ""
) : Parcelable

data class ReleaseShort (
    @Expose @SerializedName("_id") var id: String? = null,
    @Expose var release_id: String? = null,
    @Expose var device_id: String? = null,
    @Expose var date: Long = 0,
    @Expose var size: Int = 0,
    @Expose var md5: String? = null,
    @Expose var version: String? = null,
    @Expose var type: String? = null
)
/*
@Parcelize
data class ReleaseRecoveryImg (
    @Expose var size: Int = 0,
    @Expose var md5: String? = null
) : Parcelable*/