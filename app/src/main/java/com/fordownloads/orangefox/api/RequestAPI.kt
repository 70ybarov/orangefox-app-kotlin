package com.fordownloads.orangefox.api

import com.fordownloads.orangefox.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.tonyodev.fetch2.Request
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


object RequestAPI {
    interface OrangeFox {
        @GET("devices/get")
        fun device(@Query("_id") _id: String): Call<Device>
        @GET("devices")
        fun devicesAll(): Call<DeviceRequest>
        @GET("releases/get")
        fun release(@Query("_id") _id: String? = null,
                    @Query("release_id") release_id: String? = null): Call<Release>
        @GET("releases?sort=date_desc")
        fun releasesAll(@Query("device_id") device_id: String? = null,
                        @Query("version") version: String? = null,
                        @Query("type") type: String? = null,
                        @Query("limit") limit: Int? = null): Call<ReleaseRequest>
    }

    private val client = OkHttpClient.Builder().apply {
        addInterceptor {
            it.proceed(it.request().newBuilder()
                .header("app-version", "${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})")
                .header("User-Agent", "OrangeFox-app")
                .build())
        }
    }

    val gson: Gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    val orangeFoxApi: OrangeFox = Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client.build())
        .build().create(OrangeFox::class.java)
}