package com.fordownloads.orangefox.ui.scripts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.RadioButton
import com.fordownloads.orangefox.R


class RadioArrayAdapter(context: Context, val list: Array<String>, var selectedPosition: Int = 0):
        ArrayAdapter<String>(context, R.layout.list_radio, list) {

    private val inflater = LayoutInflater.from(context)
    var onItemSelectedListener: ((Int) -> Unit)? = null

    override fun getView(pos: Int, convertView: View?, parent: ViewGroup): View =
        (convertView ?: inflater.inflate(R.layout.list_radio, null)).apply {
            findViewById<RadioButton>(R.id.title).apply {
                text = list[pos]
                isChecked = pos == selectedPosition
                setOnClickListener {
                    selectedPosition = pos
                    onItemSelectedListener?.invoke(pos)
                    notifyDataSetChanged()
                }
            }
        }
}