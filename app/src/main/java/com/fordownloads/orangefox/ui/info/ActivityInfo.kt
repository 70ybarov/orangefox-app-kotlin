package com.fordownloads.orangefox.ui.info

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.TextView
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.RecyclerView
import com.fordownloads.orangefox.*
import com.fordownloads.orangefox.Const.extraDeviceId
import com.fordownloads.orangefox.Const.extraReleaseId
import com.fordownloads.orangefox.Const.extraReleaseType
import com.fordownloads.orangefox.Const.extraTitle
import com.fordownloads.orangefox.Const.extraType
import com.fordownloads.orangefox.Const.extraVersion
import com.fordownloads.orangefox.api.*
import com.fordownloads.orangefox.api.RequestAPI.gson
import com.fordownloads.orangefox.api.RequestAPI.orangeFoxApi
import com.fordownloads.orangefox.base.ActivityBase
import com.fordownloads.orangefox.databinding.ActivityInfoBinding
import com.fordownloads.orangefox.databinding.ItemListBinding
import com.fordownloads.orangefox.databinding.PageRecyclerBinding
import com.fordownloads.orangefox.ui.install.SheetInstall.createInstallSheet
import com.fordownloads.orangefox.utils.Tools
import com.fordownloads.orangefox.utils.Tools.activityCallback
import com.fordownloads.orangefox.utils.Tools.fade
import com.fordownloads.orangefox.utils.Tools.formatDate
import com.fordownloads.orangefox.utils.Tools.openUrl
import com.fordownloads.orangefox.utils.Tools.reportException
import com.fordownloads.orangefox.utils.Tools.startAnimation
import com.fordownloads.orangefox.utils.Tools.stopAnimation
import com.fordownloads.orangefox.utils.applyInsets
import com.fordownloads.orangefox.utils.visible
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Integer.max
import java.net.UnknownHostException


class ActivityInfo  : ActivityBase<ActivityInfoBinding>(R.layout.activity_info, e2e = true) {
    private var menuObj: Menu? = null
    private var menuItems = arrayListOf<Pair<Int, String>>()

    private val finishCallback = activityCallback {
        if (it.resultCode == Activity.RESULT_OK) finish()
    }

    override fun setup(state: Bundle?) {
        val type = intent.extraType

        if (type != Const.TYPE_RELEASE_ALL && type != Const.TYPE_DEVICE_INFO) {
            removeNavBarScrim()
            b.loading.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                leftMargin = dp(16)  // move shimmer down to match placement of items
                topMargin = dp(168)  // of accordion
            }

            applyInsets(b.root, toolbar = b.layout) {
                dp(16).let {
                    b.pager.pageMargin = it
                    b.pager.setPadding(it, it, it, max(bottom, it))
                }
            }
        } else
            applyInsets(b.root, toolbar = b.layout) {
                b.pager.setPadding(0, 0, 0, bottom)
            }


        b.loading.startAnimation()
        b.loadingTabs.startAnimation()

        b.title = getString(intent.extraTitle)

        with (orangeFoxApi) {
            when (type) {
                Const.TYPE_DEVICE_INFO -> device(intent.extraDeviceId).get(R.string.err_no_device) { parseDeviceInfo(it) }
                Const.TYPE_RELEASE_ALL     -> releasesAll(intent.extraDeviceId).get(R.string.err_no_rels) { parseReleaseList(it) }
                Const.TYPE_RELEASE_ONLINE  -> release(_id = intent.extraReleaseId).get { parseReleaseInfo(it) }
                Const.TYPE_RELEASE_OFFLINE -> parseReleaseInfo(Gson().fromJson(prefs.cacheRelease, Release::class.java))
                Const.TYPE_RELEASE_FIND_ID -> release(release_id = intent.extraReleaseId).get { parseReleaseInfo(it) }
                Const.TYPE_RELEASE_FIND_VERSION ->
                    releasesAll(device_id = prefs.deviceId,
                                version = intent.extraVersion,
                                type = intent.extraReleaseType).get {
                        release(it.data?.get(0)?.id?:"").get { release ->
                            parseReleaseInfo(release)
                        }
                    }
            }
        }
    }

    private val openRelease = { ib: ItemListBinding, _: Int ->
        finishCallback.launch(
            Intent(this, this::class.java)
                .putExtra(Const.EXTRA_TYPE, Const.TYPE_RELEASE_ONLINE)
                .putExtra(Const.EXTRA_RELEASE_ID, ib.item?.data)
        )
    }

    private fun parseReleaseInfo(release: Release) {
        release.url?.let { menuItems.add(Pair(R.string.rel_browser, it)) }
        addMenuItems()

        val variants = release.variants?.entrySet()?.map {
            gson.fromJson(it.value, Variant::class.java).apply { variant_name = it.key }
        } ?: run { throw  Error("Variants not found") }

        val groups = arrayListOf(Pair(getString(R.string.rel_general), arrayListOf(
            addItem(R.string.rel_vers, release.version,           R.drawable.ic_new),
            addItem(R.string.rel_type, release.type.formatType(), R.drawable.ic_wrench),
            addItem(R.string.rel_date, release.date.formatDate(), R.drawable.ic_today)
        ))) + variants.map {
            Pair(getString(R.string.rel_variant_title, it.variant_name), arrayListOf(
                addItem(R.string.rel_name, it.filename, R.drawable.ic_archive),
                addItem(R.string.rel_size, it.size.formatSize(), R.drawable.ic_sdcard)
            ))
        }

        setupViewPager {
            add(Pair(getString(R.string.rel_info),
                PageRecyclerBinding.inflate(layoutInflater).apply {
                    recycler.adapter = ExpandAdapterInfo(groups) {
                        createInstallSheet(
                            finishActivity = true,
                            release = ReleaseToInstall(
                                version = release.version?:"",
                                type    = release.type.formatType()?:"",
                                variant = variants[it])).show()
                    }
                    recycler.addItemDecoration(MarginDecor(dp(2)))
                }.root
            ))

            addHTMLTextView(R.string.rel_changes, release.changelog)
            addHTMLTextView(R.string.rel_notes, listOf(release.notes))
            addHTMLTextView(R.string.rel_bugs, release.bugs)
        }
    }

    private fun parseReleaseList(releases: ReleaseRequest) =
        releases.data?.asSequence()?.map {
            RecyclerItem(
                title = it.version ?: "",
                subtitle = it.date.formatDate(),
                icon = R.drawable.ic_archive,
                data = it.id,
                searchData = it.type
            )
        }?.partition { it.searchData == "stable" }?.let { (stable, beta) ->
            setupViewPager {
                addRecyclerView(R.string.rel_stable, stable.toMutableList(), openRelease)
                addRecyclerView(R.string.rel_beta, beta.toMutableList(), openRelease)
            }
        }?:run { b.errorMessage = getString(R.string.err_no_rels) }

    private fun parseDeviceInfo(device: Device) {
        device.url?.let { menuItems.add(Pair(R.string.rel_browser, it)) }
        device.sources?.let { menuItems.add(Pair(R.string.dev_tree, it)) }
        addMenuItems()

        setupViewPager {
            addRecyclerView(R.string.rel_info, arrayListOf(
                addItem(R.string.dev_model,      device.model_names?.joinToString(), R.drawable.ic_wrench),
                addItem(R.string.dev_code,       device.codenames?.joinToString(),   R.drawable.ic_new),
                addItem(R.string.dev_maintainer, device.maintainer?.name,            R.drawable.ic_archive),
                addItem(R.string.dev_status,
                    getString(if (device.supported) R.string.dev_maintained else R.string.dev_unmaintained),
                    R.drawable.ic_today),
            ))

            addHTMLTextView(R.string.rel_notes, listOf(device.public_notes))

            addRecyclerView(R.string.dev_support, arrayListOf(
                //addItem(R.string.dev_telegram,  device.support?.telegram_chat,   R.drawable.ic_tg),
                //addItem(R.string.dev_email,     device.support?.telegram_chat,   R.drawable.ic_tg),
                addItem(R.string.dev_chat,        device.support?.telegram_chat,   R.drawable.ic_telegram),
                addItem(R.string.dev_forum,       device.support?.forum,           R.drawable.ic_message),
                addItem(R.string.dev_instruction, device.installation_instruction, R.drawable.ic_scripts)
            )) { it, _ -> openUrl(it.item?.subtitle) }
        }
    }

    private fun Int.formatSize() = getString(R.string.size_mb, this / 1048576)
    private fun String?.formatType() =
        when (this) {
            "stable", "Stable" -> getString(R.string.rel_stable)
            "beta", "Beta" -> getString(R.string.rel_beta)
            else -> this
        }

    private fun addItem(title: Int, subtitle: String?, icon: Int) =
        RecyclerItem(getString(title), subtitle?:getString(R.string.unknown), icon)

    private fun ArrayList<Pair<String,View>>.addHTMLTextView(title: Int, list: List<String?>?) {
        if (list != null && list[0] != null)
            add(Pair(
                getString(title),
                TextView(ContextThemeWrapper(this@ActivityInfo, R.style.TextBox)).apply {
                    text = Html.fromHtml(list.joinToString("") { "<li>\t$it</li>\n" }, Html.FROM_HTML_MODE_COMPACT)
                }
            ))
    }

    private fun ArrayList<Pair<String,View>>.addRecyclerView(
                title: Int, items: MutableList<RecyclerItem>,
                listener: ((ib: ItemListBinding, _: Int) -> Unit)? = null) {
        if (items.size > 0)
            add(Pair(
                getString(title),
                RecyclerView(ContextThemeWrapper(this@ActivityInfo, R.style.RecycleView)).apply {
                    adapter = RecyclerAdapter(items, listener)
                }
            ))
    }

    private inline fun setupViewPager(init: ArrayList<Pair<String,View>>.() -> Unit) =
        b.apply {
            pager.adapter = PagerAdapterInfo(arrayListOf<Pair<String,View>>().apply { init() })
            pager.offscreenPageLimit = 4
            tabs.setupWithViewPager(pager)

            if (intent.extraType == Const.TYPE_RELEASE_OFFLINE) {
                b.loading.stopAnimation()
                b.loadingTabs.stopAnimation()
                loading.visible = false
                loadingTabs.visible = false
                pager.alpha = 1F
                tabs.alpha = 1F
            } else {
                loadingTabs.fade(0F, object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        loadingTabs.visible = false
                        b.loadingTabs.stopAnimation()
                        tabs.fade(1F)
                    }
                })
                loading.fade(0f, object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        loading.visible = false
                        b.loading.stopAnimation()
                        pager.fade(1F)
                    }
                })
            }
        }

    fun<T> Call<T>.get(customError: Int = R.string.err_no_rel, callback : ((result: T) -> Unit)) =
        enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                reportException(t, false)
                (t is UnknownHostException).also {
                    b.errorInternet = it
                    b.errorMessage = if (it) getString(R.string.err_no_internet) else t.toString()
                }
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful)
                    response.body()?.let { return callback(it) }

                val code = response.code()
                val err = getString(if (Tools.isServerError(code)) R.string.err_ise else customError)
                b.errorMessage = "$code: $err"
            }
        })

    private fun addMenuItems() =
        menuObj?.let { menuItems.forEach { i ->
            it.add(i.first)?.setOnMenuItemClickListener {
                openUrl(i.second)
                true
            }
            menuObj = null
        }}

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuObj = menu
        addMenuItems()
        return true
    }
}