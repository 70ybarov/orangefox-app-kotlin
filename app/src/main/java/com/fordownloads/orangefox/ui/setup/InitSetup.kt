package com.fordownloads.orangefox.ui.setup

import androidx.fragment.app.*
import com.fordownloads.orangefox.Const
import com.fordownloads.orangefox.Const.argStandalone
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.RecyclerItem
import com.fordownloads.orangefox.api.DeviceRequest

object InitSetup {
    fun FragmentActivity.initWelcomeScreen(isMissingPM: Boolean = false) {
        setContentView(FragmentContainerView(this).apply { id = R.id.fm })

        supportFragmentManager.commit {
            if (isMissingPM)
                replace(R.id.fm, FragmentPermissions().apply { argStandalone = true })
            else
                replace(R.id.fm, FragmentWelcome())
        }
    }

    fun cleanup() {
        devices = null
    }

    fun listDevices(response: DeviceRequest): ArrayList<RecyclerItem>? {
        val devices = arrayListOf<RecyclerItem>()

        response.data?.forEach {
            it.codenames?.filter { it in Const.DEVICE_CHECK }?.forEach { _ ->
                devices.add(0, RecyclerItem(
                    title = it.title ?: "",
                    subtitle = it.codenames?.joinToString() ?: "",
                    icon = R.drawable.ic_device_check,
                    data = it.id
                ))
            }
            devices += RecyclerItem(
                title = it.title ?: "",
                subtitle = it.codenames?.joinToString() ?: "",
                icon = R.drawable.ic_device,
                data = it.id,
                searchData = "${it.model_names?.joinToString(" ")} ${it.codenames?.joinToString(" ")} ${it.title}"
            )
        } ?: run { return null }

        return devices
    }

    var devices: ArrayList<RecyclerItem>? = null
}