package com.fordownloads.orangefox.ui.main

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.commit
import com.fordownloads.orangefox.Const
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.base.ActivityBase
import com.fordownloads.orangefox.databinding.ActivityMainBinding
import com.fordownloads.orangefox.ui.install.FragmentInstall
import com.fordownloads.orangefox.ui.logs.FragmentLogs
import com.fordownloads.orangefox.ui.scripts.FragmentScripts
import com.fordownloads.orangefox.ui.setup.InitSetup
import com.fordownloads.orangefox.ui.setup.InitSetup.initWelcomeScreen
import com.fordownloads.orangefox.utils.Tools.hasStoragePermission
import com.fordownloads.orangefox.utils.Tools.hasStoragePermissionScoped
import com.fordownloads.orangefox.utils.applyInsets
import java.lang.Integer.max


class ActivityMain: ActivityBase<ActivityMainBinding>(e2e = true, customInit = true) {
    private lateinit var fInstall: FragmentInstall
    private lateinit var fScripts: FragmentScripts
    private lateinit var fLogs: FragmentLogs
    private var welcomeMode = false
    private var shortcutMode = false

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        window.setBackgroundDrawableResource(R.color.fox_background)

        if (prefs.deviceCodename == null) {
            initWelcomeScreen()
            welcomeMode = true
            return
        } else if (!hasStoragePermission() || !hasStoragePermissionScoped()) {
            initWelcomeScreen(isMissingPM = true)
            welcomeMode = true
            return
        }

        createNotificationChannel()
        b = ActivityMainBinding.inflate(layoutInflater)
        applyInsets(b.root, toolbar = b.appToolbar) {
            val bottomMin = max(bottom, dp(16))
            b.bottomNavWrap.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                leftMargin = left + dp(16)
                rightMargin = right + dp(16)
                bottomMargin = bottomMin
            }
            b.navFrame.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                leftMargin = left
                rightMargin = right
                bottomMargin = bottomMin + if (shortcutMode) 0 else dp(44)
            }
            b.navFrame.setPadding(0, top, 0, 0)
        }

        setContentView(b.root)
        setSupportActionBar(b.appToolbar)

        removeNavBarScrim()
        prepareBottomNav()
    }

    override fun onNewIntent(intent: Intent) {
        when {
            (!intent.hasExtra("shortcut") && shortcutMode) -> {
                setIntent(intent)
                recreate()
            }
            intent.hasExtra(Const.FORCE_RELOAD) -> fInstall.fetch(force = true)
        }
        super.onNewIntent(intent)
    }

    override fun onRestoreInstanceState(state: Bundle) {
        super.onRestoreInstanceState(state)
        if(!welcomeMode)
            b.bottomNavigation.selectedItemId = state.getInt(Const.STATE_SELECTED)
    }

    override fun onSaveInstanceState(state: Bundle) {
        super.onSaveInstanceState(state)
        if(!welcomeMode)
            state.putInt(Const.STATE_SELECTED, b.bottomNavigation.selectedItemId)
    }

    private fun prepareBottomNav() {
        val fm = supportFragmentManager

        fm.commit {
            fm.findFragmentByTag(Const.FRAGMENT_INSTALL)
                ?. let { fInstall = it as FragmentInstall }
                ?: run { add(R.id.navFrame, FragmentInstall().also
                { fInstall = it }, Const.FRAGMENT_INSTALL) }
            fm.findFragmentByTag(Const.FRAGMENT_SCRIPTS)
                ?. let { fScripts = it as FragmentScripts }
                ?: run { add(R.id.navFrame, FragmentScripts().also
                { fScripts = it }, Const.FRAGMENT_SCRIPTS).hide(fScripts) }
            fm.findFragmentByTag(Const.FRAGMENT_LOGS)
                ?. let { fLogs = it as FragmentLogs }
                ?: run { add(R.id.navFrame, FragmentLogs().also
                { fLogs = it }, Const.FRAGMENT_LOGS).hide(fLogs) }
        }

        b.bottomNavigation.setOnItemSelectedListener { item ->
            fm.commit {
                setCustomAnimations(R.anim.scale, 0)
                when (item.itemId) {
                    R.id.scripts -> show(fScripts).hide(fInstall).hide(fLogs)
                    R.id.logs -> show(fLogs).hide(fInstall).hide(fScripts)
                    else -> show(fInstall).hide(fScripts).hide(fLogs)
                }
            }
            true
        }

        intent.getStringExtra("shortcut")?.let {
            b.bottomNavigation.selectedItemId = if (it == "logs") R.id.logs else R.id.scripts
            b.shortcutMode = true
            shortcutMode = true
        }
    }

    private fun createNotificationChannel() {
        val notifyMan = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        var channel = NotificationChannel(Const.CHANNEL_UPDATE,
            getString(R.string.notif_ch_update), NotificationManager.IMPORTANCE_HIGH).apply {
            description = getString(R.string.notif_ch_update_desc)
        }
        notifyMan.createNotificationChannel(channel)

        channel = NotificationChannel(Const.CHANNEL_DOWNLOAD,
            getString(R.string.notif_ch_download), NotificationManager.IMPORTANCE_LOW).apply {
            description = getString(R.string.notif_ch_download_desc)
        }
        notifyMan.createNotificationChannel(channel)

        channel = NotificationChannel(Const.CHANNEL_DOWNLOAD_STATUS,
            getString(R.string.notif_ch_download_status), NotificationManager.IMPORTANCE_HIGH).apply {
            description = getString(R.string.notif_ch_download_status_desc)
        }
        notifyMan.createNotificationChannel(channel)
    }
}