package com.fordownloads.orangefox.ui.info

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.fordownloads.orangefox.RecyclerAdapter
import com.fordownloads.orangefox.RecyclerItem
import com.fordownloads.orangefox.databinding.ItemExpandBinding

class ExpandAdapterInfo(
        val items: List<Pair<String, ArrayList<RecyclerItem>>>, // bruh
        val listener: (position: Int) -> Unit
    ) : RecyclerView.Adapter<ExpandAdapterInfo.ViewHolder>() {

    class ViewHolder(val b: ItemExpandBinding) : RecyclerView.ViewHolder(b.root)

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemExpandBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.b.apply {
            title = items[position].first
            recycler.adapter = RecyclerAdapter(items[position].second)

            if (position == 0 || items.size == 2)
                expansionLayout.expand(false)

            if (position > 0)
                setInstallTap { listener(position - 1) }
        }
    }
}

class MarginDecor(private val margin: Int) : ItemDecoration() {
    override fun getItemOffsets(rect: Rect, v: View, p: RecyclerView, s: RecyclerView.State) {
        if (p.getChildLayoutPosition(v) != 0)
            rect.top = margin
    }
}

class MarginDecorHorizontal(private val margin: Int) : ItemDecoration() {
    override fun getItemOffsets(rect: Rect, v: View, p: RecyclerView, s: RecyclerView.State) {
        if (p.getChildLayoutPosition(v) != 0)
            rect.left = margin
        rect.right = margin
    }
}