package com.fordownloads.orangefox.ui.logs

import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.Menu
import android.view.MenuItem
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.base.ActivityBase
import com.fordownloads.orangefox.databinding.ActivityLogBinding
import com.fordownloads.orangefox.utils.Tools.fade
import com.fordownloads.orangefox.utils.Tools.reportException
import com.fordownloads.orangefox.utils.Tools.share
import com.fordownloads.orangefox.utils.Tools.showSnackbar
import com.fordownloads.orangefox.utils.applyInsets
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.util.zip.ZipFile
import kotlin.concurrent.thread


class ActivityLog : ActivityBase<ActivityLogBinding>(R.layout.activity_log, e2e = true) {
    private var itemPos = -1
    private lateinit var fileName: String
    private lateinit var log: File

    override fun setup(state: Bundle?) {
        fileName = intent.getStringExtra("file")?: run {
            throw IllegalArgumentException("Calling activity without file name") }
        itemPos = intent.getIntExtra("pos", -1) //when main activity is recreated, it loses position so we should keep it

        log = File(Environment.getExternalStorageDirectory(), "Fox/logs/$fileName")

        applyInsets(b.root, toolbar = b.appToolbar, recycler = b.log, fab = b.fab)

        supportActionBar?.title = fileName.split(".")[0]

        b.fab.setOnClickListener {
            val atStart = b.scroll.scrollY == 0
            b.scroll.smoothScrollTo(0, if (atStart) b.log.height else 0, 500)
            b.expandToolbar.setExpanded(!atStart, true)
        }
        showLog()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menu.clear()
        menuInflater.inflate(R.menu.share, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.share -> share(prefs, fileName, log)
            R.id.delete ->
                if (log.delete()) {
                    setResult(RESULT_OK, Intent().putExtra("pos", itemPos))
                    finish()
                } else showSnackbar(R.string.err_file_delete, b.fab)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun readStream(stream: InputStream, size: Long): String {
        if (size > 102400)
            stream.reader().readLines().let { lines ->
                return lines.take(100).joinToString("\n") +
                        "\n\n\n<--- ${getString(R.string.log_lines_skipped, 1500)} --->\n\n\n" +
                        lines.takeLast(1500).joinToString("\n")
            }
        else
            return stream.reader().readText()
    }

    private fun showLog() = thread(true) {
        var text: String
        if (log.exists())
            try {
                when (log.absolutePath.takeLast(4)) {
                    ".log" -> text = readStream(log.inputStream(), log.length())
                    ".zip" -> ZipFile(log).use {
                        val file = it.entries().nextElement()
                        text = readStream(it.getInputStream(file), file.size)
                    }
                    else -> text = getString(R.string.err_file_format)
                }
            } catch (e: IOException) {
                reportException(e, true)
                text = e.localizedMessage ?: "I/O Error"
            }
        else
            text = getString(R.string.err_file_notfound)

        runOnUiThread {
            b.log.text = text
            b.loadingLayout.fade(0f)
        }
    }
}