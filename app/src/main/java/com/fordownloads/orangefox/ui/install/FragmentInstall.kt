package com.fordownloads.orangefox.ui.install

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.fordownloads.orangefox.*
import com.fordownloads.orangefox.Const.extraForceReload
import com.fordownloads.orangefox.api.*
import com.fordownloads.orangefox.api.RequestAPI.gson
import com.fordownloads.orangefox.api.RequestAPI.orangeFoxApi
import com.fordownloads.orangefox.base.FragmentBase
import com.fordownloads.orangefox.databinding.FragmentInstallBinding
import com.fordownloads.orangefox.ui.info.ActivityInfo
import com.fordownloads.orangefox.ui.info.MarginDecor
import com.fordownloads.orangefox.ui.settings.ActivitySettings
import com.fordownloads.orangefox.utils.Tools
import com.fordownloads.orangefox.utils.Tools.doYeetIfNoPM
import com.fordownloads.orangefox.utils.Tools.fade
import com.fordownloads.orangefox.utils.Tools.formatDate
import com.fordownloads.orangefox.utils.Tools.getSystemProperty
import com.fordownloads.orangefox.utils.Tools.jsonToObjSafe
import com.fordownloads.orangefox.utils.visible
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.Math.random
import java.net.SocketException
import java.net.UnknownHostException
import kotlin.math.floor
import androidx.recyclerview.widget.LinearSnapHelper

import androidx.recyclerview.widget.SnapHelper
import com.fordownloads.orangefox.ui.info.MarginDecorHorizontal


class FragmentInstall : FragmentBase<FragmentInstallBinding>(R.layout.fragment_install) {
    private lateinit var pulse: Animator
    private val items = arrayListOf<CardItem>()
    private var calls = arrayListOf<Call<*>>()

    private fun addCard(titleRes: Int, icon: Int, table: Map<Int, String?>, infoListener: View.OnClickListener) =
        items.add(CardItem(getString(titleRes), icon, table, infoListener))


    override fun setup(state: Bundle?) {
        b.recycler.apply {
            adapter = CardAdapterInstall(items)
            addItemDecoration(MarginDecorHorizontal(dp(8)))
            LinearSnapHelper().attachToRecyclerView(this)
        }

        b.refresh.apply {
            setColorSchemeResources(R.color.fox_accent)
            setProgressViewOffset(true, 2, 64)
            setOnRefreshListener {
                b.recycler.fade(0F, object : AnimatorListenerAdapter() {
                    @SuppressLint("NotifyDataSetChanged")
                    override fun onAnimationEnd(animation: Animator) {
                        b.recycler.alpha = 0F
                        fetch(true)
                    }
                })
            }
            isEnabled = false
        }

        b.btnRefresh.setOnClickListener { fetch(true) }

        b.setAllBuildsClick {
            startActivity(Intent(activity, ActivityInfo::class.java)
                .putExtra(Const.EXTRA_TYPE, Const.TYPE_RELEASE_ALL)
                .putExtra(Const.EXTRA_DEVICE_ID, prefs.deviceId)
                .putExtra(Const.EXTRA_TITLE, R.string.rels_activity))
        }

        b.setDeviceInfoClick {
            startActivity(Intent(activity, ActivityInfo::class.java)
                .putExtra(Const.EXTRA_TYPE, Const.TYPE_DEVICE_INFO)
                .putExtra(Const.EXTRA_DEVICE_ID, prefs.deviceId)
                .putExtra(Const.EXTRA_TITLE, R.string.dev_info))
        }

        b.deviceCodename = prefs.deviceCodename

        pulse = AnimatorInflater.loadAnimator(activity, R.animator.pulse)
        pulse.setTarget(b.shimmer)

        fetch()
    }

    fun fetch(force: Boolean = false) {
        calls.clear()
        b.recycler.fade(0F)
        b.errorMessage = null
        pulse.start()
        resetState()
        b.refresh.isEnabled = false

        val id = prefs.deviceId ?: run {
            b.errorInternet = false
            b.errorMessage = getString(R.string.err_dev_not_selected)
            return
        }

        orangeFoxApi.releasesAll(device_id = id, limit = 1).get(R.string.err_no_rels) {
            parseReleaseList(it, force)
        }
    }

    private fun parseReleaseInstalled() =
        jsonToObjSafe<ReleaseInstalled>(Const.RELEASE_JSON.readText())?.let { release ->
            addCard(R.string.rel_current, R.drawable.ic_installed, mapOf(
                    R.string.rel_vers to release.version,
                    R.string.rel_type to release.type.formatType(),
                    R.string.rel_commit to release.commit,
                    R.string.rel_date to release.date?.formatDate(),
                    R.string.rel_variant to release.variant
                )) {
                    startActivity(Intent(activity, ActivityInfo::class.java).apply {
                        putExtra(Const.EXTRA_TITLE, R.string.rel_current)
                        release.release_id?.let {
                            putExtra(Const.EXTRA_TYPE, Const.TYPE_RELEASE_FIND_ID)
                            putExtra(Const.EXTRA_RELEASE_ID, release.release_id)
                        } ?: run {
                            putExtra(Const.EXTRA_TYPE, Const.TYPE_RELEASE_FIND_VERSION)
                            putExtra(Const.EXTRA_VERSION, release.version)
                            putExtra(Const.EXTRA_RELEASE_TYPE, release.type)
                        }
                    })
                }
        }

    private fun parseReleaseList(releases: ReleaseRequest, force: Boolean) {
        val id = releases.data?.get(0)?.id ?: run {
            return cancelRequests(getString(R.string.err_no_rels))
        }

        val cacheRelease =
            if (prefs.cacheReleaseId == id && !force)
                jsonToObjSafe<Release>(prefs.cacheRelease)
            else null

        if (cacheRelease == null) {
            prefs.cacheReleaseId = id
            orangeFoxApi.release(id).get {
                prefs.cacheRelease = gson.toJson(it)
                parseReleaseInfo(it)
            }
        } else
            parseReleaseInfo(cacheRelease)
    }

    private fun parseReleaseInfo(release: Release) {
        addCard(R.string.rel_last, R.drawable.ic_new, mapOf(
            R.string.rel_vers to release.version,
            R.string.rel_type to release.type.formatType(),
            R.string.rel_date to release.date.formatDate()
        )
        ) {
            startActivity(
                Intent(activity, ActivityInfo::class.java)
                    .putExtra(Const.EXTRA_TYPE, Const.TYPE_RELEASE_OFFLINE)
                    .putExtra(Const.EXTRA_TITLE, R.string.rel_last)
            )
        }
        finishParsing()
        showHelpMessage()
    }

    private fun String?.formatType() =
        when (this) {
            "stable", "Stable" -> getString(R.string.rel_stable)
            "beta", "Beta" -> getString(R.string.rel_beta)
            else -> this
        }

    fun<T> Call<T>.get(customError: Int = R.string.err_no_rel, callback : ((result: T) -> Unit)) {
        calls += this
        enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                Tools.reportException(t, false)
                Log.e(Const.TAG, "Failed to fetch: $t")
                Handler(Looper.getMainLooper()).postDelayed({
                    when (t) {
                        is UnknownHostException, is SocketException -> {
                            jsonToObjSafe<Release>(prefs.cacheRelease)?.let {
                                b.helpCardTitle = getString(R.string.err_card_no_internet)
                                b.helpCardSubTitle = getString(R.string.err_no_internet_short)
                                parseReleaseInfo(it)
                                return@postDelayed
                            }
                            b.errorInternet = true
                            cancelRequests(getString(R.string.err_no_internet))
                        }
                        is IOException -> return@postDelayed
                        else -> {
                            b.errorInternet = false
                            b.errorMessage = t.toString()
                        }
                    }
                }, 400) //make transitions between error/loading cards smoother
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    response.body()?.let { callback(it) }
                    return
                }

                val code = response.code()
                val err = getString(if (Tools.isServerError(code)) R.string.err_ise else customError)
                cancelRequests("$code: $err")
            }
        })
    }

    private fun resetState() {
        items.clear()
        b.apply {
            helpCardClick = null
            helpCardTitle = null
            refresh.isRefreshing = false
            shimmer.visible = true
        }
    }

    private fun showHelpMessage() {
        if (!prefs.helpMessages || b.helpCardTitle != null) return

        /*try {
            val request: Request =
                Builder().url("https://gitlab.com/OrangeFox/misc/appdev/updates/-/raw/master/version_id.txt")
                    .build()
            val response: Response<*> = client.newCall(request).execute()
            if (response.isSuccessful) {
                val newVersion: Int = response.body().string().trim().toInt()
                if (newVersion == 0) {
                    requireActivity().runOnUiThread {
                        _annoyTitle.setText(R.string.very_old_version)
                        _annoyText.setText(R.string.very_old_version_desc)
                        _annoyCard.setVisibility(View.VISIBLE)
                    }
                    return
                } else if (BuildConfig.VERSION_CODE < newVersion) {
                    requireActivity().runOnUiThread {
                        _annoyTitle.setText(R.string.update_msg)
                        _annoyText.setText(R.string.update_msg_desc)
                        rootView.findViewById(R.id.swipeCard).setOnClickListener { v ->
                            startActivityForResult(
                                Intent(
                                    activity,
                                    UpdateActivity::class.java
                                ), 600
                            )
                        }
                        _annoyCard.setVisibility(View.VISIBLE)
                    }
                    return
                }
            }
        } catch (e: Exception) {
            Tools.reportException(e)
        }*/

        val name = resources.getStringArray(R.array.message_list)

        var id = 0

        if (!prefs.dialogMIUIShown) {
            if (getSystemProperty("ro.miui.ui.version.name") == "")
                prefs.dialogMIUIShown = true
            else id = 2
        }

        if (id != 2)
            id = when {
                !prefs.updatesEnabled -> 0
                !Const.DIR_FOX_MAGISK.exists() && Const.LOG_LAST.exists() -> 1
                else -> floor(random() * (name.size - 4.0) + 3.0).toInt()
            }

        val desc = resources.getStringArray(R.array.message_list_desc)
        val url = resources.getStringArray(R.array.message_list_url)

        b.helpCardTitle = name[id]
        b.helpCardSubTitle = desc[id]

        b.setHelpCardClick {
            when (url[id]) {
                "null" -> {}
                "settings" -> startActivity(Intent(activity, ActivitySettings::class.java))
                else -> startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url[id])))
            }
            b.helpCardTitle = null
        }
    }

    private fun finishParsing() {
        if (Const.RELEASE_JSON.exists())
            parseReleaseInstalled()
        b.shimmer.visible = false
        pulse.cancel()
        b.recycler.adapter?.notifyDataSetChanged()
        b.recycler.fade(1F)
        b.refresh.isEnabled = true
        b.refresh.isRefreshing = false
    }

    private fun cancelRequests(error: String) {
        if (calls.size == 0) return
        calls.forEach { it.cancel() }
        calls.clear()
        resetState()
        b.errorMessage = error
        b.refresh.isEnabled = true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.settings)
            startActivity(Intent(activity, ActivitySettings::class.java))
        return false
    }
}