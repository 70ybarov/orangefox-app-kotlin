package com.fordownloads.orangefox.ui.settings

import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.commit
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.base.ActivityBase

class ActivitySettings : ActivityBase<Nothing>(e2e = true, customInit = true) {
    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        setContentView(FragmentContainerView(this).apply { id = R.id.fm })
        if (state == null)
            supportFragmentManager.commit { replace(R.id.fm, FragmentSettings()) }
    }
}