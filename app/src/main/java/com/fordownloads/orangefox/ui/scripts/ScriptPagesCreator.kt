package com.fordownloads.orangefox.ui.scripts

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.CheckBox
import androidx.core.view.children
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.RecyclerItem
import com.fordownloads.orangefox.databinding.PageGenericActionBinding
import com.fordownloads.orangefox.databinding.PageInstallActionBinding
import com.fordownloads.orangefox.databinding.PageMountActionBinding
import com.fordownloads.orangefox.utils.Tools
import java.lang.IllegalStateException


class ScriptPagesCreator(
    private val activity : Activity,
    private val addCallback: (item: RecyclerItem) -> Unit) {

    private val shake = AnimationUtils.loadAnimation(activity, R.anim.shake)
    private fun getString(resId: Int) = activity.getString(resId)
    private fun getStringArray(resId: Int) = activity.resources.getStringArray(resId)
    private fun createTab(vg: ViewGroup, adapter: ArrayAdapter<String>) =
        PageGenericActionBinding.inflate(activity.layoutInflater, vg, false).apply {
            list.adapter = adapter
        }
    private fun add(title: Int, subtitle: String, icon: Int, data: String) =
        addCallback(RecyclerItem(getString(title), subtitle, icon, data))

    fun installTab(vg: ViewGroup, addZipFunction: View.OnClickListener) =
        PageInstallActionBinding.inflate(activity.layoutInflater, vg, false).apply {
            onAddClick = addZipFunction
        }.root

    fun backupTab(vg: ViewGroup) : View {
        val backupParts = getStringArray(R.array.scripts_backup_values)
        val adapter = ArrayAdapter(activity, R.layout.list_check, getStringArray(R.array.scripts_backup))

        return createTab(vg, adapter).apply {
            hint1 = getString(R.string.script_backup_name)
            text1 = Tools.getBackupFileName()

            setOnAddClick {
                val text = text1 ?: ""
                val name = if (text.isBlank()) Tools.getBackupFileName() else text.trim()
                var arg = "backup "
                var argUser = ""
                var empty = true

                list.children.filterIsInstance<CheckBox>().forEachIndexed { i, check ->
                    if (check.isChecked) {
                        arg += backupParts[i]
                        if (i >= 5) // don't check digest/compress
                            argUser += "\n${check.text}"
                        else { //partitions
                            argUser += "${check.text}, "
                            empty = false
                        }
                    }
                }
                if (empty)
                    list.startAnimation(shake)
                else
                    add(R.string.script_backup, "$name\n${argUser.trim(' ', ',')}",
                        R.drawable.ic_backup, "$arg $name")
            }
        }.root
    }

    fun wipeTab(vg: ViewGroup) : View {
        val wipeList = getStringArray(R.array.scripts_wipe)

        return createTab(vg, ArrayAdapter(activity, R.layout.list_check, wipeList)).apply {
            setOnAddClick {
                val cmd = list.children.filterIsInstance<CheckBox>()
                    .filter { it.isChecked }.map { it.text.toString() }
                if (cmd.none())
                    list.startAnimation(shake)
                else
                    add(R.string.script_wipe, cmd.joinToString(", "),
                        R.drawable.ic_delete, cmd.joinToString("\n") {"wipe ${it.lowercase()}"})
            }
        }.root
    }

    fun mountTab(vg: ViewGroup) =
        PageMountActionBinding.inflate(activity.layoutInflater, vg, false).apply {
            setOnRWClick {
                add(R.string.script_rw, "System", R.drawable.ic_remount, "remountrw")
            }

            setOnUnmountClick {
                val part = partition ?: ""
                if (part.isBlank())
                    textBox.startAnimation(shake)
                else
                    add(R.string.script_umount, part,
                        R.drawable.ic_umount, "umount ${part.trim().lowercase()}")
            }

            setOnMountClick {
                val part = partition ?: ""
                if (part.isBlank())
                    textBox.startAnimation(shake)
                else
                    add(R.string.script_mount, part,
                        R.drawable.ic_mount, "mount ${part.trim().lowercase()}")
            }
        }.root

    fun rebootTab(vg: ViewGroup) : View {
        val rebootList = getStringArray(R.array.scripts_reboot)
        val rebootListCmd = getStringArray(R.array.scripts_reboot_values)
        val adapter = RadioArrayAdapter(activity, rebootList)

        return createTab(vg, adapter).apply {
            setOnAddClick {
                add(R.string.script_reboot, rebootList[adapter.selectedPosition],
                    R.drawable.ic_refresh,
                    "reboot ${rebootListCmd[adapter.selectedPosition]}")
            }
        }.root
    }

    fun etcTab(vg: ViewGroup) : View {
        val etcListInput = getStringArray(R.array.scripts_etc_values)
        val adapter = RadioArrayAdapter(activity, getStringArray(R.array.scripts_etc))

        return createTab(vg, adapter).apply {
            hint1 = etcListInput[0]
            adapter.onItemSelectedListener = {
                when (it) {
                    3, 4 -> { hint1 = null; hint2 = null }
                    5    -> { hint1 = etcListInput[it]; hint2 = getString(R.string.script_var_val) }
                    else -> { hint1 = etcListInput[it]; hint2 = null }
                }
            }
            setOnAddClick {
                val text = (text1 ?: "").trim()
                val textArg = text2 ?: ""

                if (adapter.selectedPosition !in 3..4 && text.isEmpty())
                    textBoxes.startAnimation(shake)
                else
                    when (adapter.selectedPosition) {
                        0 -> add(R.string.script_cmd, text, R.drawable.ic_terminal, "cmd $text")
                        1 -> add(R.string.script_mkdir, text, R.drawable.ic_folder, "mkdir $text")
                        2 -> add(R.string.script_print, text, R.drawable.ic_message,"print $text")
                        3 -> add(R.string.script_sideload, getString(R.string.script_no_params), R.drawable.ic_sideload, "sideload")
                        4 -> add(R.string.script_fixperms, getString(R.string.script_no_params), R.drawable.ic_wrench, "fixperms")
                        5 -> add(R.string.script_set, "$text = $textArg", R.drawable.ic_equals, "set $text ${textArg.trim()}")
                        else -> throw IllegalStateException()
                    }
            }
        }.root
    }
}