package com.fordownloads.orangefox.ui.settings

import android.content.Context
import android.util.AttributeSet
import android.view.ContextThemeWrapper
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.Preference
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.base.SheetBase
import com.fordownloads.orangefox.components.SmartListView
import com.fordownloads.orangefox.ui.scripts.RadioArrayAdapter

class MirrorPreference(context: Context, attrs: AttributeSet?) : SheetPreference(context, attrs) {
    override val entries: Array<String> = context.resources.getStringArray(R.array.mirror)
    override val entryValues: Array<String> = context.resources.getStringArray(R.array.mirror_values)
    override val subtitle = context.resources.getString(R.string.pref_mirror_desc)

    override fun getSummary() = super.getSummary() ?: entries[0].substring(5)
    override fun setSummary(summary: CharSequence?) = //cut emoji flag (one flag = 4 bytes + space)
        super.setSummary(summary?.toString()?.substring(5))

    init { init() }
}

class ThemePreference(context: Context, attrs: AttributeSet?) : SheetPreference(context, attrs) {
    override val entries: Array<String> = context.resources.getStringArray(R.array.theme)
    override val entryValues: Array<String> = context.resources.getStringArray(R.array.theme_values)

    override fun getSummary() = super.getSummary() ?: entries[0]
    override var onItemSelectedListener: ((Int) -> Unit)? = {
        AppCompatDelegate.setDefaultNightMode(if (it == 0) AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM else it)
    }

    init { init() }
}

abstract class SheetPreference(context: Context, attrs: AttributeSet?) : Preference(context, attrs) {
    abstract val entries: Array<String>
    abstract val entryValues: Array<String>
    open val subtitle: String? = null
    open var onItemSelectedListener: ((Int) -> Unit)? = null

    private lateinit var radioAdapter: RadioArrayAdapter
    private var sheet = SheetBase(context)

    override fun onClick() { sheet.show() }
    init { layoutResource = R.layout.pref_item }

    override fun onSetInitialValue(defaultValue: Any?) {
        val defaultId = entryValues.indexOf(getPersistedString(defaultValue as String?))
        if (defaultId != -1)
            summary = entries[defaultId]
        radioAdapter.selectedPosition = defaultId
        radioAdapter.notifyDataSetChanged()
    }

    fun init() {
        radioAdapter = RadioArrayAdapter(context, entries)
        radioAdapter.onItemSelectedListener = {
            sheet.dismiss()
            summary = entries[it]
            persistString(entryValues[it])
            onItemSelectedListener?.invoke(it)
        }

        val listView = SmartListView(ContextThemeWrapper(context, R.style.SheetListView)).apply {
            divider = null
            adapter = radioAdapter
        }

        sheet.body.title = title.toString()
        sheet.body.subtitle = subtitle
        sheet.body.inner.addView(listView)
    }
}