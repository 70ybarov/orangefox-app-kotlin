package com.fordownloads.orangefox.ui.scripts

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.UP
import androidx.recyclerview.widget.ItemTouchHelper.DOWN
import androidx.recyclerview.widget.ItemTouchHelper.START
import androidx.recyclerview.widget.ItemTouchHelper.END
import androidx.recyclerview.widget.RecyclerView
import com.fordownloads.orangefox.RecyclerItem
import com.fordownloads.orangefox.databinding.ItemScriptBinding
import java.util.*

interface ItemTouchHelperAdapter {
    fun onItemMove(fromPosition: Int, toPosition: Int): Boolean
    fun onItemDismiss(position: Int)
}

class AdapterScriptsTouchHelper(private val adapter: ItemTouchHelperAdapter) :
    ItemTouchHelper.Callback() {

    override fun getMovementFlags(r: RecyclerView, v: RecyclerView.ViewHolder) =
        makeMovementFlags(UP or DOWN, START or END)

    override fun onSwiped(v: RecyclerView.ViewHolder, i: Int) = adapter.onItemDismiss(v.adapterPosition)

    override fun onMove(r: RecyclerView, src: RecyclerView.ViewHolder, trg: RecyclerView.ViewHolder)
            = if (src.itemViewType != trg.itemViewType) false
              else adapter.onItemMove(src.adapterPosition, trg.adapterPosition)
}

class AdapterScripts(
    private val items: ArrayList<RecyclerItem>,
    private val clickListener: ((ib: ItemScriptBinding, position: Int) -> Unit)? = null,
    private val dragStartListener: ((viewHolder: RecyclerView.ViewHolder?) -> Unit)?,
    private val dismissListener: ((position: Int) -> Unit)?
) :
    RecyclerView.Adapter<AdapterScripts.ViewHolder>(), ItemTouchHelperAdapter {

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(ItemScriptBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: RecyclerItem = items[position]
        holder.b.item = item
        holder.b.handle.setOnTouchListener { _: View?, event: MotionEvent ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN)
                dragStartListener?.invoke(holder)
            false
        }
    }

    inner class ViewHolder(val b: ItemScriptBinding) : RecyclerView.ViewHolder(b.root) {
        init {
            b.root.setOnClickListener { clickListener?.invoke(b, layoutPosition) }
        }
    }

    override fun onItemDismiss(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
        dismissListener?.invoke(position)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(items, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }
}
