package com.fordownloads.orangefox

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager

class Prefs(private val prefs: SharedPreferences) {
    constructor(context: Context) : this(PreferenceManager.getDefaultSharedPreferences(context))

    var setupCompleted
        get() = prefs.getBoolean("setupCompleted", false)
        set(value) = prefs.edit { putBoolean("setupCompleted", value) }

    // Cache / info
    var cacheDevice
        get() = prefs.getString("cacheDevice", "")
        set(value) = prefs.edit { putString("cacheDevice", value) }

    var cacheRelease
        get() = prefs.getString("cacheRelease", "{}")
        set(value) = prefs.edit { putString("cacheRelease", value) }

    var cacheReleaseId
        get() = prefs.getString("cacheReleaseID", null)
        set(value) = prefs.edit { putString("cacheReleaseID", value) }

    var deviceName
        get() = prefs.getString("deviceName", null)
        set(value) = prefs.edit { putString("deviceName", value) }

    var deviceCodename
        get() = prefs.getString("deviceCodename", null)
        set(value) = prefs.edit { putString("deviceCodename", value) }

    var deviceId
        get() = prefs.getString("deviceId", null)
        set(value) = prefs.edit { putString("deviceId", value) }

    var mirrorCode
        get() = prefs.getString("mirrorCode", null) ?: "US"
        set(value) = prefs.edit { putString("mirrorCode", value) }

    // Auto Updates
    var updatesEnabled
        get() = prefs.getBoolean("updatesEnabled", false)
        set(value) = prefs.edit { putBoolean("updatesEnabled", value) }

    var updatesBeta
        get() = prefs.getBoolean("updatesBeta", false)
        set(value) = prefs.edit { putBoolean("updatesBeta", value) }

    var updatesAutoInstall
        get() = prefs.getBoolean("updatesAutoInstall", false)
        set(value) = prefs.edit { putBoolean("updatesAutoInstall", value) }

    var updatesLimitedConnections
        get() = prefs.getBoolean("updatesLimitedConnections", true)
        set(value) = prefs.edit { putBoolean("updatesLimitedConnections", value) }

    // UI
    var helpMessages
        get() = prefs.getBoolean("helpMessages", true)
        set(value) = prefs.edit { putBoolean("helpMessages", value) }

    var theme
        get() = prefs.getString("theme", null) ?: "auto"
        set(value) = prefs.edit { putString("theme", value) }

    // Help things
    var dialogShareShown
        get() = prefs.getBoolean("dialogShareShown", false)
        set(value) = prefs.edit { putBoolean("dialogShareShown", value) }

    var dialogMIUIShown
        get() = prefs.getBoolean("dialogMIUIShown", false)
        set(value) = prefs.edit { putBoolean("dialogMIUIShown", value) }
}