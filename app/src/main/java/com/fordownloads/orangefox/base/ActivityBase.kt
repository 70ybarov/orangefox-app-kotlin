package com.fordownloads.orangefox.base

import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.fordownloads.orangefox.Prefs
import com.fordownloads.orangefox.R
import com.fordownloads.orangefox.utils.Tools.doYeetIfNoPM

abstract class ActivityBase<VB: ViewDataBinding>(val layout: Int = 0, val customInit: Boolean = false,
                                                 val e2e: Boolean = false): AppCompatActivity(), IBase {
    internal lateinit var b: VB
    lateinit var prefs: Prefs
    override fun dp (i: Int) = ((resources?.displayMetrics?.density?:1F) * i).toInt()

    fun removeNavBarScrim() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            window.isNavigationBarContrastEnforced = false
    }

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        prefs = Prefs(this)

        if (e2e) {
            WindowCompat.setDecorFitsSystemWindows(window, false)
            window.navigationBarColor = 0
            window.statusBarColor = 0
        }

        if (customInit || doYeetIfNoPM()) return

        b = DataBindingUtil.bind(layoutInflater.inflate(layout, null, false)) ?: run {
            throw Error("Activity binding error")
        }
        setContentView(b.root)
        setSupportActionBar(b.root.findViewById(R.id.appToolbar))
        setup(state)
    }

    protected open fun setup(state: Bundle?) = Unit

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}