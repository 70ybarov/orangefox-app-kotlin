package com.fordownloads.orangefox

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationManagerCompat
import com.fordownloads.orangefox.Const.extraAction
import com.fordownloads.orangefox.utils.Tools.getORS
import com.topjohnwu.superuser.Shell
import com.topjohnwu.superuser.io.SuFile

class BroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        Log.i("OFRReceiver", "Received: ${intent.extraAction}")
        when (intent.extraAction) {
            Const.NotifyAction.RebootRecovery ->
                if (!Shell.su("reboot recovery").exec().isSuccess)
                    Toast.makeText(context, R.string.err_reboot_notify, Toast.LENGTH_LONG).show()

            Const.NotifyAction.DeleteORS ->
                if (SuFile(getORS()).delete())
                    NotificationManagerCompat.from(context).cancel(Const.NOTIFY_DOWNLOAD_SAVED)
                else
                    Toast.makeText(context, R.string.err_ors_delete, Toast.LENGTH_LONG).show()

            else -> throw IllegalStateException("NotifyAction is illegal")
        }
    }
}